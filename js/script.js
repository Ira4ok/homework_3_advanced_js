let table = document.createElement('table');
table.className = 'table';

for (let i = 1; i < 31; i++) {
    let tableRow = document.createElement('tr');
    for (let j = 1; j < 31; j++) {
        let tableCell = document.createElement('td');
        tableCell.className = 'table-cell';

        tableRow.appendChild(tableCell);
        table.appendChild(tableRow);
    }
}
    document.body.appendChild(table);

// let tableCells = document.querySelectorAll('td');
//
// document.body.addEventListener('click', changeCellColor, false);
//
// function changeCellColor(event) {
//     if (event.target.tagName === 'TD') {
//         event.target.classList.toggle('black-cell')
//     } else if (event.currentTarget === event.target) {
//         tableCells.forEach(function (item) {
//             item.classList.toggle('black-cell')
//         })
//     }
// }



/*-----------------------------2nd  version - split into 2 different event-targets-----------------------------------*/
let tableCells = document.querySelectorAll('td');

table.addEventListener('click', (e) => {
    let target = e.target;

    if (target.tagName === 'TD') {
        target.classList.toggle('black-cell');
    }
    e.stopPropagation();
});

document.body.addEventListener('click', changeColor, false);


function changeColor() {
    tableCells.forEach(function (item) {
            item.classList.toggle('black-cell')
    });
}
